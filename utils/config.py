import yaml


def load_config_file(config_file):
    with open(config_file, "r") as f:
        config = yaml.safe_load(f)
        access_token = config["access_token"]
    return access_token
