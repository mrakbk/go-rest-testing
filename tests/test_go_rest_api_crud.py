from random import choice
from utils.config import load_config_file
from faker import Faker
import lorem
import pytest
import requests


# GLOBAL
access_token = load_config_file("../config.yml")
article_base_url = "https://gorest.co.in/public/v2/posts"
user_base_url = "https://gorest.co.in/public/v2/users"


# TEST DATA
@pytest.fixture(scope="module")
def headers():
    headers = {
        "Content-Type": "application/json",
        'Authorization': f'Bearer {access_token}'
    }
    return headers


@pytest.fixture(scope="module")
def user(headers):
    payload = {
        "name": Faker().name(),
        "email": Faker().email(),
        "gender": choice(["female", "male"]),
        "status": "active"
    }
    response = requests.post(url=user_base_url, headers=headers, json=payload)
    return response.json()


@pytest.fixture(scope="module")
def article(user):
    article = {
        "user_id": user["id"],
        "title": lorem.sentence(),
        "body": lorem.paragraph()
    }
    return article


@pytest.fixture(scope="module")
def posted_article(article, headers):
    payload = {
        "user_id": article["user_id"],
        "title": article["title"],
        "body": article["body"]
    }
    response = requests.post(url=article_base_url, headers=headers, json=payload)
    return response


# CRUD: Create
def test_is_new_post_created(posted_article):
    assert posted_article.status_code == 201


# CRUD: Read
def test_new_post_can_be_read(posted_article, headers):
    new_post_id = posted_article.json()['id']
    get_post_response = requests.get(url=f"{article_base_url}/{new_post_id}", headers=headers)
    assert get_post_response.status_code == 200
    assert posted_article.json()['title'] == get_post_response.json()['title']
    assert posted_article.json()['body'] == get_post_response.json()['body']


# CRUD: Update
def test_is_new_post_updated(posted_article, headers):
    payload = {
        "title": lorem.sentence(),
        "body": lorem.paragraph()
    }
    new_post_id = posted_article.json()['id']
    update_post_response = requests.put(url=f"{article_base_url}/{new_post_id}", headers=headers, json=payload)
    assert update_post_response.status_code == 200


# CRUD: Delete
def test_is_new_post_deleted(posted_article, headers):
    new_post_id = posted_article.json()['id']
    delete_post_response = requests.delete(url=f"{article_base_url}/{new_post_id}", headers=headers)
    assert delete_post_response.status_code == 204
