# Go REST testing



## Description

The project was created to practice REST API testing with Python (requests package) based on [Go REST](https://gorest.co.in/).

### CRUD

File `test_go_rest_api_crud.py`

### Flow

File `test_go_rest_api_flow.py`
